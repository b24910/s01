
// Activity;
// 1.
"create variable with the value of new Array"

// 2.
"get the index of the array then call the index 0 to access its value"


// 3.
"get the length of the array then minus 1"


// 4.
"indexof()"


// 5.
"map, foreach, for of"


// 6.
"map"


// 7.
"filter"

// 8.
"includes"

// 9.
false
// 10.
true

// functional coding
// 1.



function addToEnd(Arr, val){
    if(typeof val == "string"){
        const newArr = Arr
        newArr.push(val)
        return newArr
    }else{
        return "error - can only add strings to an array"
    }
}

function addToStart (Arr, val){
    if(typeof val == "string"){
        const newArr = Arr
        newArr.unshift(val)
        return newArr
    }else{
        return "error - can only add strings to an array"
    }
}

function elementChecker(Arr, val){
    if(Arr.length){
        return Arr.contain(val)
    }else{
        return "error - passed in array is empty"
    }
}

const students = ["John", "Joe", "Jane", "Jessie"];
console.log(addToEnd(students, 'Ryan'))
console.log(addToStart(students, "Tess"))

function checkAllStringEnding(Arr, val){
    if(Arr.length){
        return Arr.contain(val)
    }else{
        return "error - passed in array is empty"
    }
}